create database stocks;
-- int id; String stockTicker; double price; int volume; String buyOrSell;int statusCode;

use stocks;
create table stockinfo(
	id  int auto_increment Primary Key,
    stockTicker varchar(40),
    price double,
    volume int,
    buyOrSell varchar(40),
    statusCode int
    );
    
-- AMZN, AAPL, C, NFLX
insert into stockinfo values (201,"AMZN",4500,100,"BUY",0);
insert into stockinfo values (202,"NFLX",5000,200, "SELL",1);
insert into stockinfo values (203,"HULU",6000,300, "BUY",0);
insert into stockinfo values (204,"TARGET",7000,400, "BUY",2);

